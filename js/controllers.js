sunguard.controller("productController", ['$scope', '$http', 'ProductService', function($scope, $http, ProductService) {
	
	$scope.products = [
		{ 
			'ID': 0,
			'productname':'Samsung Grand 2',
			'costperunit': 16500,
			'sellingprice': 18000,
			'stock': 50
		},
		{ 
			'ID': 1,
			'productname':'Nokia Lumia 525',
			'costperunit': 6999,
			'sellingprice': 7250,
			'stock': 20
		},
		{ 
			'ID': 2,
			'productname':'Motorola G II',
			'costperunit': 7600,
			'sellingprice': 8500,
			'stock': 25
		},
		{ 
			'ID': 3,
			'productname':'iPhone 6+',
			'costperunit': 35000,
			'sellingprice': 38000,
			'stock': 10
		},
		{ 
			'ID': 4,
			'productname':'Samsung Tab 2',
			'costperunit': 16500,
			'sellingprice': 18500,
			'stock': 50
		},
		
	];
	
	$scope.showScreen = function(__screen) {
		$scope.templatePath = ProductService.getTemplate(__screen);
	};
	
	$scope.startActivity = function(event) {
		$scope.event = fetchJSON.loadJSON(_path + _dataFile);
		$scope.event.then(function(event) {
			StoryService.parseJSON(event);
		}, function(status) {
			console.log(status);
		});
	};
	
	$scope.addRow = function(){
		$ID = $scope.products.length;
		$scope.productname = $("#pName").val();
		$scope.costperunit = $("#cPrice").val();
		$scope.sellingprice = $("#sPrice").val();
		$scope.stock = $("#stock").val();
		if($scope.costperunit < $scope.sellingprice) {
			$scope.products.push(
				{
					'ID':$ID,
					'productname':$scope.productname,
					'costperunit':$scope.costperunit,
					'sellingprice':$scope.sellingprice,
					'stock':$scope.stock
				}
			);
			$scope.ID='';
			$scope.productname='';
			$scope.costperunit='';
			$scope.sellingprice='';
			$scope.stock='';
			$scope.templatePath = ProductService.getTemplate('home');
			$('#myTabs li').removeClass("active");
			$('#home-tab').parent().addClass("active");
		} else {
			alert("Selling Price should be greater than Cost Price.");
		}
	};
	

	$scope.editRow = function(ID){
		var index = -1;
		var prodArr = eval( $scope.products );
		$scope.ID = prodArr[ID].ID;
		$scope.productname = prodArr[ID].productname;
		$scope.costperunit = prodArr[ID].costperunit;
		$scope.sellingprice = prodArr[ID].sellingprice;
		$scope.stock = prodArr[ID].stock;
		$scope.templatePath = ProductService.getTemplate('editproduct');
		$('#myTabs li').removeClass("active");
		$('#editprod-tab').parent().addClass("active");
	};
	
	
	$scope.updateRow = function(ID){
		var products = $scope.products;
		var prodArr = eval( $scope.products );
		$scope.productname = $("#pName").val();
		$scope.sellingprice = $("#sPrice").val();
		$costPrice = $scope.costperunit;
		$sellingPrice = $scope.sellingprice;
		if($costPrice < $sellingPrice) {
			for (var i = 0; i < products.length; i++) {
				if (products[i].ID === ID) {
					products[i].ID = ID;
					products[i].productname = $scope.productname;
					products[i].costperunit = $costPrice;
					products[i].sellingprice = $sellingPrice;
					products[i].stock = $scope.stock;
					break;
				}
			};
			$scope.ID='';
			$scope.productname='';
			$scope.costperunit='';
			$scope.sellingprice='';
			$scope.stock='';
			$scope.templatePath = ProductService.getTemplate('home');
			$('#myTabs li').removeClass("active");
			$('#home-tab').parent().addClass("active");
		} else {
			alert("Selling Price should be greater than Cost Price.");
		}
		
	};
	

	$scope.removeRow = function(ID){				
		var index = -1;		
		var prodArr = eval( $scope.products );
		for( var i = 0; i < prodArr.length; i++ ) {
			if( prodArr[i].ID === ID ) {
				index = i;
				break;
			}
		}
		if( index === -1 ) {
			alert( "Something gone wrong" );
		}
		$scope.products.splice( index, 1 );		
	};
	
	$('#myTabs a').click(function (e) {
		//e.preventDefault()
		$(this).tab('show')
	})
	
	$scope.templatePath = ProductService.getTemplate('home');
	
}]);