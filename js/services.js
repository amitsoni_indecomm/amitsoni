sunguard.config(['$httpProvider', function ($httpProvider) {    
	$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
}]);

sunguard.factory("ProductService", ['$rootScope',
function($rootScope) {
	return {
		getTemplate : function(templateType) {
			switch(templateType) {
				case 'home':
					templatePath = "partials/home.html";
					break;
				case 'addproduct':
					templatePath = "partials/addProduct.html";
					break;
				case 'editproduct':
					templatePath = "partials/editProduct.html";
					break;
			}
			return templatePath;
		}
	};
}]);


/*
****This is Method 2 for routing****

sunguard.config(['$routeProvider',
	function($routeProvider) {
		$routeProvider.when('/addProduct', {
			templateUrl: 'partials/addProduct.html',
			controller: 'AddProductController'
		}).when('/editProduct', {
			templateUrl: 'partials/editProduct.html',
			controller: 'EditProductController'
		}).when('/home', {
			templateUrl: 'partials/home.html',
			controller: 'HomeController'
		}).otherwise({
			redirectTo: '/home'
		});
	}
]);
sunguard.controller('HomeController', function($scope) {
	$scope.message = 'This is Add new order screen';
});
sunguard.controller('AddProductController', function($scope) {
	$scope.message = 'This is Add new order screen';
});
sunguard.controller('EditProductController', function($scope) {
	$scope.message = 'This is Show orders screen';
});
*/